# shellcheck shell=bash

AddPackage \
		htop \
		lynis \
			arch-audit \
		man-db \
		man-pages \
		moreutils \
		unzip \
		vim \
		xdg-user-dirs \
		yadm

# shellcheck shell=bash

AddPackage \
		openrgb

CopyFile /etc/openrgb/OpenRGB.json

cat >>"$(CreateFile /etc/systemd/system/rgb.service)" <<-'EOF'
	[Unit]
	Before=multi-user.target
	Wants=dev-hidraw0.device
	After=dev-hidraw0.device

	[Service]
	Type=oneshot
	User=rgb
	WorkingDirectory=/var/local/rgb
	ExecStart=/usr/bin/openrgb --config /etc/openrgb --device 0 --color FFFFFF,FFFFFF,FFFFFF,000000,FFFFFF,FFFFFF,FFFFFF,FFFFFF
	ExecStop=/usr/bin/rm -rf /var/local/rgb/.config/OpenRGB
	ExecStop=/usr/bin/rmdir --ignore-fail-on-non-empty /var/local/rgb/.config

	[Install]
	WantedBy=multi-user.target
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-rgb.preset)" <<-'EOF'
	enable rgb.service
EOF

cat >>"$(CreateFile /etc/udev/rules.d/99-rgb.rules)" <<-'EOF'
	KERNEL=="hidraw*", SUBSYSTEM=="hidraw", TAG+="systemd", RUN+="/usr/bin/setfacl -m u:rgb:rw $env{DEVNAME}"
EOF

# shellcheck shell=bash

AddPackage \
		gutenprint \
			foomatic-db-gutenprint-ppds

SetFileProperty /etc/cups/classes.conf mode 600

cat >>"$(CreateFile /etc/systemd/system-preset/00-printer.preset)" <<-'EOF'
	enable cups.service
EOF

# shellcheck shell=bash

cat >>"$(CreateFile /etc/locale.conf)" <<-'EOF'
	LANG=fr_FR.UTF-8
EOF

patch "$(GetPackageOriginalFile glibc /etc/locale.gen)" <<-'EOF'
	@@ -168,7 +168,7 @@
	 #en_SC.UTF-8 UTF-8  
	 #en_SG.UTF-8 UTF-8  
	 #en_SG ISO-8859-1  
	-#en_US.UTF-8 UTF-8  
	+en_US.UTF-8 UTF-8  
	 #en_US ISO-8859-1  
	 #en_ZA.UTF-8 UTF-8  
	 #en_ZA ISO-8859-1  
	@@ -239,7 +239,7 @@
	 #fr_CA ISO-8859-1  
	 #fr_CH.UTF-8 UTF-8  
	 #fr_CH ISO-8859-1  
	-#fr_FR.UTF-8 UTF-8  
	+fr_FR.UTF-8 UTF-8  
	 #fr_FR ISO-8859-1  
	 #fr_FR@euro ISO-8859-15  
	 #fr_LU.UTF-8 UTF-8  
EOF

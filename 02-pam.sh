# shellcheck shell=bash

AddPackage \
		libpwquality

patch "$(GetPackageOriginalFile pambase /etc/pam.d/system-auth)" <<-'EOF'
	@@ -18,7 +18,8 @@
	 account    required                    pam_time.so

	 -password  [success=1 default=ignore]  pam_systemd_home.so
	-password   required                    pam_unix.so          try_first_pass nullok shadow
	+password   required                    pam_pwquality.so     retry=3 minlen=10 dcredit=-1 ucredit=-1 lcredit=-1 ocredit=-1 enforce_for_root
	+password   required                    pam_unix.so          try_first_pass nullok shadow use_authtok
	 password   optional                    pam_permit.so

	 -session   optional                    pam_systemd_home.so
EOF

patch "$(GetPackageOriginalFile util-linux /etc/pam.d/su)" <<-'EOF'
	@@ -3,7 +3,7 @@
	 # Uncomment the following line to implicitly trust users in the "wheel" group.
	 #auth           sufficient      pam_wheel.so trust use_uid
	 # Uncomment the following line to require a user to be in the "wheel" group.
	-#auth           required        pam_wheel.so use_uid
	+auth            required        pam_wheel.so use_uid
	 auth            required        pam_unix.so
	 account         required        pam_unix.so
	 session	        required        pam_unix.so
EOF

patch "$(GetPackageOriginalFile util-linux /etc/pam.d/su-l)" <<-'EOF'
	@@ -3,7 +3,7 @@
	 # Uncomment the following line to implicitly trust users in the "wheel" group.
	 #auth           sufficient      pam_wheel.so trust use_uid
	 # Uncomment the following line to require a user to be in the "wheel" group.
	-#auth           required        pam_wheel.so use_uid
	+auth            required        pam_wheel.so use_uid
	 auth            required        pam_unix.so
	 account         required        pam_unix.so
	 session	        required        pam_unix.so
EOF

# shellcheck shell=bash

AddPackage \
		ark \
			p7zip \
			unrar \
		dolphin \
		firefox \
		firefox-i18n-en-us \
		firefox-i18n-fr \
		gimp \
		gwenview \
		kcalc \
		konsole \
		kwalletmanager \
		okular \
		spectacle \
		synology-drive \
		vlc \
			zvbi

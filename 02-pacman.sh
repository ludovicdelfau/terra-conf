# shellcheck shell=bash

AddPackage \
		pacman-contrib \
		pkgstats

patch "$(GetPackageOriginalFile pacman /etc/pacman.conf)" <<-'EOF'
	@@ -30,7 +30,7 @@

	 # Misc options
	 #UseSyslog
	-#Color
	+Color
	 #NoProgressBar
	 CheckSpace
	 #VerbosePkgLists
	@@ -89,11 +89,19 @@
	 #[multilib-testing]
	 #Include = /etc/pacman.d/mirrorlist

	-#[multilib]
	-#Include = /etc/pacman.d/mirrorlist
	+[multilib]
	+Include = /etc/pacman.d/mirrorlist

	 # An example of a custom package repository.  See the pacman manpage for
	 # tips on creating your own repositories.
	 #[custom]
	 #SigLevel = Optional TrustAll
	 #Server = file:///home/custompkgs
	+
	+[aur]
	+SigLevel = Optional TrustAll
	+Server = file:///var/local/aur
	+
	+[lizardbyte]
	+SigLevel = Optional
	+Server = https://github.com/LizardByte/pacman-repo/releases/latest/download
EOF

patch "$(GetPackageOriginalFile pacman-mirrorlist /etc/pacman.d/mirrorlist)" <<-EOF
	@@ -261,8 +257,7 @@
	 #Server = https://mirroir.labhouse.fr/arch/\$repo/os/\$arch
	 #Server = http://mirror.lastmikoi.net/archlinux/\$repo/os/\$arch
	 #Server = http://archlinux.mailtunnel.eu/\$repo/os/\$arch
	-#Server = ${ARCH_MIRROR}
	+Server = ${ARCH_MIRROR}
	-#Server = https://f.matthieul.dev/mirror/archlinux/\$repo/os/\$arch
	 #Server = http://mir.archlinux.fr/\$repo/os/\$arch
	 #Server = http://mirrors.celianvdb.fr/archlinux/\$repo/os/\$arch
	 #Server = https://mirrors.celianvdb.fr/archlinux/\$repo/os/\$arch
EOF

patch "$(GetPackageOriginalFileTo pacman-contrib /usr/lib/systemd/system/paccache.service /etc/systemd/system/paccache.service)" <<-EOF
	@@ -3,7 +3,7 @@

	 [Service]
	 Type=oneshot
	-ExecStart=/usr/bin/paccache -r
	+ExecStart=/usr/bin/paccache -c /var/cache/pacman/pkg -c /var/local/aur -rvuk2
	 # Lowering priority
	 OOMScoreAdjust=1000
	 Nice=19
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-pacman.preset)" <<-'EOF'
	enable paccache.timer
EOF

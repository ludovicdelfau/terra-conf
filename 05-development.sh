# shellcheck shell=bash

AddPackage \
		agda \
			agda-stdlib \
		code \
		gcc \
		git \
		git-lfs \
		python-pipenv \
		rustup

# shellcheck shell=bash

AddPackage \
		archlinux-wallpaper

cat >>"$(CreateFile /etc/sddm.conf.d/00-numlock.conf)" <<-'EOF'
	[General]
	Numlock=on
EOF

cat >>"$(CreateFile /etc/sddm.conf.d/00-theme.conf)" <<-'EOF'
	[Theme]
	Current=breeze
	CursorTheme=breeze_cursors
EOF

cat >>"$(CreateFile /usr/share/sddm/themes/breeze/theme.conf.user)" <<-'EOF'
	[General]
	background=/usr/share/backgrounds/archlinux/conference.png
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-sddm.preset)" <<-'EOF'
	enable sddm.service
EOF

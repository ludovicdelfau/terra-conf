# shellcheck shell=bash

AddPackage \
		amd-ucode \
		refind \
		refind-theme-regular-git \
		sbsigntools \
		shim-signed

iconv -t UCS-2 >>"$(CreateFile /boot/EFI/refind/BOOT.CSV)" <<-EOF
	shimx64.efi,rEFInd Boot Manager,,This is the boot entry for rEFInd
EOF

patch "$(GetPackageOriginalFileTo refind /usr/share/refind/refind.conf-sample /boot/EFI/refind/refind.conf)" <<-EOF
	@@ -10,7 +10,7 @@
	 # shortcut key. If no matching shortcut is found, rEFInd displays its
	 # menu with no timeout.
	 #
	-timeout 20
	+timeout 10

	 # Set the logging level. When set to 0, rEFInd does not log its actions.
	 # When set to 1 or above, rEFInd creates a file called refind.log in
	@@ -71,8 +71,7 @@
	 #  all         - all of the above
	 # Default is none of these (all elements active)
	 #
	-#hideui singleuser
	-#hideui all
	+hideui hints,editor

	 # Set the name of a subdirectory in which icons are stored. Icons must
	 # have the same names they have in the standard directory. The directory
	@@ -183,10 +182,7 @@
	 # values often don't.
	 # Default is "0 0" (use the system default resolution, usually 800x600).
	 #
	-#resolution 1024 768
	-#resolution 1440 900
	-#resolution 3
	-#resolution max
	+resolution max

	 # Enable touch screen support. If active, this feature enables use of
	 # touch screen controls (as on tablets). Note, however, that not all
	@@ -238,7 +234,7 @@
	 #   windows - Microsoft Windows
	 # Default value: osx
	 #
	-#use_graphics_for osx,linux
	+use_graphics_for linux,windows

	 # Which non-bootloader tools to show on the tools line, and in what
	 # order to display them:
	@@ -275,7 +271,7 @@
	 # To completely disable scanning for all tools, provide a showtools line
	 # with no options.
	 #
	-#showtools shell, bootorder, gdisk, memtest, mok_tool, apple_recovery, windows_recovery, about, hidden_tags, reboot, exit, firmware, fwupdate
	+showtools about,shutdown,reboot,firmware

	 # Additional directories to scan for tools. You may specify a directory
	 # alone or a volume identifier plus pathname. The default is to scan no
	@@ -330,7 +326,7 @@
	 # On UEFI PCs, default is internal,external,optical,manual
	 # On Macs, default is internal,hdbios,external,biosexternal,optical,cd,manual
	 #
	-#scanfor internal,external,optical,manual,firmware
	+scanfor external,optical,manual

	 # By default, rEFInd relies on the UEFI firmware to detect BIOS-mode boot
	 # devices. This sometimes doesn't detect all the available devices, though.
	@@ -546,11 +542,7 @@
	 # set different defaults for different times of day.
	 # The default behavior is to boot the previously-booted OS.
	 #
	-#default_selection 1
	-#default_selection Microsoft
	-#default_selection "+,bzImage,vmlinuz"
	-#default_selection Maintenance 23:30 2:00
	-#default_selection "Maintenance,macOS" 1:00 2:30
	+default_selection "Arch Linux"

	 # Enable VMX bit and lock the CPU MSR if unlocked.
	 # On some Intel Apple computers, the firmware does not lock the MSR 0x3A.
	@@ -747,3 +739,27 @@
	     firmware_bootnum 80
	     disabled
	 }
	+
	+menuentry "Arch Linux" {
	+    icon    /EFI/refind/themes/refind-theme-regular/icons/128-48/os_arch.png
	+    volume  boot
	+    loader  /vmlinuz-linux-zen
	+    initrd  /initramfs-linux-zen.img
	+    options "root=LABEL=arch rootfstype=ext4 rootflags=${MOUNT_OPTIONS[arch]} resume=LABEL=swap rw quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_level=3"
	+    submenuentry "Boot using fallback ramdisk image" {
	+        initrd /initramfs-linux-zen-fallback.img
	+    }
	+    submenuentry "Boot to systemd-boot" {
	+        loader /EFI/systemd/grubx64.efi
	+        initrd
	+        options
	+    }
	+}
	+
	+menuentry "Windows" {
	+    icon   /EFI/refind/themes/refind-theme-regular/icons/128-48/os_windows.png
	+    volume boot
	+    loader /EFI/Microsoft/Boot/bootmgfw.efi
	+}
	+
	+include themes/refind-theme-regular/theme.conf
EOF

CopyFile /boot/EFI/refind/themes/refind-theme-regular/local/icons/128-48/bg_dark.png
CopyFile /boot/EFI/refind/themes/refind-theme-regular/local/icons/128-48/selection_dark-big.png
CopyFile /boot/EFI/refind/themes/refind-theme-regular/local/icons/128-48/selection_dark-small.png

patch "$(GetPackageOriginalFile refind-theme-regular-git /boot/EFI/refind/themes/refind-theme-regular/theme.conf)" <<-'EOF'
	@@ -21,7 +21,7 @@

	 #BACKGROUND IMAGE

	-banner themes/refind-theme-regular/icons/128-48/bg.png
	+#banner themes/refind-theme-regular/icons/128-48/bg.png
	 #banner themes/refind-theme-regular/icons/256-96/bg.png
	 #banner themes/refind-theme-regular/icons/384-144/bg.png
	 #banner themes/refind-theme-regular/icons/512-192/bg.png
	@@ -29,10 +29,11 @@
	 #banner themes/refind-theme-regular/icons/256-96/bg_dark.png
	 #banner themes/refind-theme-regular/icons/384-144/bg_dark.png
	 #banner themes/refind-theme-regular/icons/512-192/bg_dark.png
	+banner themes/refind-theme-regular/local/icons/128-48/bg_dark.png

	 #SELECTION IMAGE

	-selection_big themes/refind-theme-regular/icons/128-48/selection-big.png
	+#selection_big themes/refind-theme-regular/icons/128-48/selection-big.png
	 #selection_big themes/refind-theme-regular/icons/256-96/selection-big.png
	 #selection_big themes/refind-theme-regular/icons/384-144/selection-big.png
	 #selection_big themes/refind-theme-regular/icons/512-192/selection-big.png
	@@ -40,8 +41,9 @@
	 #selection_big themes/refind-theme-regular/icons/256-96/selection_dark-big.png
	 #selection_big themes/refind-theme-regular/icons/384-144/selection_dark-big.png
	 #selection_big themes/refind-theme-regular/icons/512-192/selection_dark-big.png
	+selection_big themes/refind-theme-regular/local/icons/128-48/selection_dark-big.png

	-selection_small themes/refind-theme-regular/icons/128-48/selection-small.png
	+#selection_small themes/refind-theme-regular/icons/128-48/selection-small.png
	 #selection_small themes/refind-theme-regular/icons/256-96/selection-small.png
	 #selection_small themes/refind-theme-regular/icons/384-144/selection-small.png
	 #selection_small themes/refind-theme-regular/icons/512-192/selection-small.png
	@@ -49,6 +51,7 @@
	 #selection_small themes/refind-theme-regular/icons/256-96/selection_dark-small.png
	 #selection_small themes/refind-theme-regular/icons/384-144/selection_dark-small.png
	 #selection_small themes/refind-theme-regular/icons/512-192/selection_dark-small.png
	+selection_small themes/refind-theme-regular/local/icons/128-48/selection_dark-small.png

	 #FONT
EOF
SetFileProperty /boot/EFI/refind/themes/refind-theme-regular/theme.conf mode 644

iconv -t UCS-2 >>"$(CreateFile /boot/EFI/systemd/BOOT.CSV)" <<-EOF
	shimx64.efi,Linux Boot Manager,,This is the boot entry for Linux
EOF

cat >>"$(CreateFile /boot/loader/entries/arch.conf)" <<-EOF
	title   Arch Linux
	linux   /vmlinuz-linux-zen
	initrd  /initramfs-linux-zen.img
	options root=LABEL=arch rootfstype=ext4 rootflags=${MOUNT_OPTIONS[arch]} resume=LABEL=swap rw quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_level=3
EOF

cat >>"$(CreateFile /boot/loader/entries/arch-fallback.conf)" <<-EOF
	title   Arch Linux (Fallback)
	linux   /vmlinuz-linux-zen
	initrd  /initramfs-linux-zen-fallback.img
	options root=LABEL=arch rootfstype=ext4 rootflags=${MOUNT_OPTIONS[arch]} resume=LABEL=swap rw quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_level=3
EOF

cat >>"$(CreateFile /boot/loader/loader.conf)" <<-'EOF'
	default      arch
	timeout      10
	console-mode max
	editor       no
EOF

cat >>"$(CreateFile /etc/pacman.d/hooks/99-linux-zen.hook)" <<-'EOF'
	[Trigger]
	Type = Package
	Operation = Upgrade
	Target = linux-zen

	[Trigger]
	Type = Path
	Operation = Upgrade
	Target = usr/lib/initcpio/*

	[Action]
	Description = Signing Linux ZEN kernel...
	When = PostTransaction
	Exec = /usr/local/bin/mok-sign /boot/vmlinuz-linux-zen
EOF

cat >>"$(CreateFile /etc/pacman.d/hooks/99-refind.hook)" <<-'EOF'
	[Trigger]
	Type = Package
	Operation = Upgrade
	Target = refind

	[Action]
	Description = Updating rEFInd...
	When = PostTransaction
	Exec = /usr/local/bin/refind-update
EOF

cat >>"$(CreateFile /etc/pacman.d/hooks/99-shim.hook)" <<-'EOF'
	[Trigger]
	Type = Package
	Operation = Upgrade
	Target = shim-signed

	[Action]
	Description = Updating shim...
	When = PostTransaction
	Exec = /usr/local/bin/shim-update
EOF

cat >>"$(CreateFile /etc/pacman.d/hooks/99-systemd-boot.hook)" <<-'EOF'
	[Trigger]
	Type = Package
	Operation = Upgrade
	Target = systemd

	[Action]
	Description = Updating systemd-boot...
	When = PostTransaction
	Exec = /usr/local/bin/systemd-boot-update
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-boot.preset)" <<-'EOF'
	disable systemd-boot-update.service
EOF

cat >>"$(CreateFile /usr/local/share/refind-sbat.csv)" <<-'EOF'
	sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
	refind,1,Terra,refind,1,https://www.rodsbooks.com/refind/
EOF

CopyFile /usr/local/bin/boot-entries-create 755
CopyFile /usr/local/bin/mok-create 755
CopyFile /usr/local/bin/mok-sign 755
CopyFile /usr/local/bin/refind-update 755
CopyFile /usr/local/bin/shim-update 755
CopyFile /usr/local/bin/systemd-boot-update 755

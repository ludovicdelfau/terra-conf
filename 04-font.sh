# shellcheck shell=bash

AddPackage \
		noto-fonts \
			noto-fonts-cjk \
			noto-fonts-emoji \
			noto-fonts-extra

CreateLink /etc/fonts/conf.d/10-hinting-slight.conf ../../../usr/share/fontconfig/conf.avail/10-hinting-slight.conf
CreateLink /etc/fonts/conf.d/10-scale-bitmap-fonts.conf ../../../usr/share/fontconfig/conf.avail/10-scale-bitmap-fonts.conf
CreateLink /etc/fonts/conf.d/10-yes-antialias.conf ../../../usr/share/fontconfig/conf.avail/10-yes-antialias.conf
CreateLink /etc/fonts/conf.d/20-unhint-small-vera.conf ../../../usr/share/fontconfig/conf.avail/20-unhint-small-vera.conf
CreateLink /etc/fonts/conf.d/30-metric-aliases.conf ../../../usr/share/fontconfig/conf.avail/30-metric-aliases.conf
CreateLink /etc/fonts/conf.d/40-nonlatin.conf ../../../usr/share/fontconfig/conf.avail/40-nonlatin.conf
CreateLink /etc/fonts/conf.d/45-generic.conf ../../../usr/share/fontconfig/conf.avail/45-generic.conf
CreateLink /etc/fonts/conf.d/45-latin.conf ../../../usr/share/fontconfig/conf.avail/45-latin.conf
CreateLink /etc/fonts/conf.d/46-noto-mono.conf ../../../usr/share/fontconfig/conf.avail/46-noto-mono.conf
CreateLink /etc/fonts/conf.d/46-noto-sans.conf ../../../usr/share/fontconfig/conf.avail/46-noto-sans.conf
CreateLink /etc/fonts/conf.d/46-noto-serif.conf ../../../usr/share/fontconfig/conf.avail/46-noto-serif.conf
CreateLink /etc/fonts/conf.d/48-spacing.conf ../../../usr/share/fontconfig/conf.avail/48-spacing.conf
CreateLink /etc/fonts/conf.d/49-sansserif.conf ../../../usr/share/fontconfig/conf.avail/49-sansserif.conf
CreateLink /etc/fonts/conf.d/50-user.conf ../../../usr/share/fontconfig/conf.avail/50-user.conf
CreateLink /etc/fonts/conf.d/51-local.conf ../../../usr/share/fontconfig/conf.avail/51-local.conf
CreateLink /etc/fonts/conf.d/60-generic.conf ../../../usr/share/fontconfig/conf.avail/60-generic.conf
CreateLink /etc/fonts/conf.d/60-latin.conf ../../../usr/share/fontconfig/conf.avail/60-latin.conf
CreateLink /etc/fonts/conf.d/65-fonts-persian.conf ../../../usr/share/fontconfig/conf.avail/65-fonts-persian.conf
CreateLink /etc/fonts/conf.d/65-nonlatin.conf ../../../usr/share/fontconfig/conf.avail/65-nonlatin.conf
CreateLink /etc/fonts/conf.d/66-noto-mono.conf ../../../usr/share/fontconfig/conf.avail/66-noto-mono.conf
CreateLink /etc/fonts/conf.d/66-noto-sans.conf ../../../usr/share/fontconfig/conf.avail/66-noto-sans.conf
CreateLink /etc/fonts/conf.d/66-noto-serif.conf ../../../usr/share/fontconfig/conf.avail/66-noto-serif.conf
CreateLink /etc/fonts/conf.d/69-unifont.conf ../../../usr/share/fontconfig/conf.avail/69-unifont.conf
CreateLink /etc/fonts/conf.d/80-delicious.conf ../../../usr/share/fontconfig/conf.avail/80-delicious.conf
CreateLink /etc/fonts/conf.d/90-synthetic.conf ../../../usr/share/fontconfig/conf.avail/90-synthetic.conf

CreateLink /etc/fonts/conf.d/69-urw-bookman.conf ../../../usr/share/fontconfig/conf.default/69-urw-bookman.conf
CreateLink /etc/fonts/conf.d/69-urw-c059.conf ../../../usr/share/fontconfig/conf.default/69-urw-c059.conf
CreateLink /etc/fonts/conf.d/69-urw-d050000l.conf ../../../usr/share/fontconfig/conf.default/69-urw-d050000l.conf
CreateLink /etc/fonts/conf.d/69-urw-fallback-backwards.conf ../../../usr/share/fontconfig/conf.default/69-urw-fallback-backwards.conf
CreateLink /etc/fonts/conf.d/69-urw-fallback-generics.conf ../../../usr/share/fontconfig/conf.default/69-urw-fallback-generics.conf
CreateLink /etc/fonts/conf.d/69-urw-fallback-specifics.conf ../../../usr/share/fontconfig/conf.default/69-urw-fallback-specifics.conf
CreateLink /etc/fonts/conf.d/69-urw-gothic.conf ../../../usr/share/fontconfig/conf.default/69-urw-gothic.conf
CreateLink /etc/fonts/conf.d/69-urw-nimbus-mono-ps.conf ../../../usr/share/fontconfig/conf.default/69-urw-nimbus-mono-ps.conf
CreateLink /etc/fonts/conf.d/69-urw-nimbus-roman.conf ../../../usr/share/fontconfig/conf.default/69-urw-nimbus-roman.conf
CreateLink /etc/fonts/conf.d/69-urw-nimbus-sans.conf ../../../usr/share/fontconfig/conf.default/69-urw-nimbus-sans.conf
CreateLink /etc/fonts/conf.d/69-urw-p052.conf ../../../usr/share/fontconfig/conf.default/69-urw-p052.conf
CreateLink /etc/fonts/conf.d/69-urw-standard-symbols-ps.conf ../../../usr/share/fontconfig/conf.default/69-urw-standard-symbols-ps.conf
CreateLink /etc/fonts/conf.d/69-urw-z003.conf ../../../usr/share/fontconfig/conf.default/69-urw-z003.conf

CreateLink /etc/fonts/conf.d/10-sub-pixel-rgb.conf ../../../usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf
CreateLink /etc/fonts/conf.d/11-lcdfilter-default.conf ../../../usr/share/fontconfig/conf.avail/11-lcdfilter-default.conf
CreateLink /etc/fonts/conf.d/70-no-bitmaps.conf ../../../usr/share/fontconfig/conf.avail/70-no-bitmaps.conf

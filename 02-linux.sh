# shellcheck shell=bash

AddPackage \
		linux-zen \
			linux-firmware \
			mkinitcpio

patch "$(GetPackageOriginalFile mkinitcpio /etc/mkinitcpio.conf)" <<-'EOF'
	@@ -52,7 +52,7 @@
	 #
	 ##   NOTE: If you have /usr on a separate partition, you MUST include the
	 #    usr and fsck hooks.
	-HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block filesystems fsck)
	+HOOKS=(base systemd autodetect microcode modconf kms block keyboard sd-vconsole fsck filesystems)

	 # COMPRESSION
	 # Use this to compress the initramfs image. By default, zstd compression
EOF

patch "$(GetPackageOriginalFileTo mkinitcpio /usr/share/mkinitcpio/hook.preset /etc/mkinitcpio.d/linux-zen.preset)" <<-'EOF'
	@@ -1,16 +1,16 @@
	-# mkinitcpio preset file for the '%PKGBASE%' package
	+# mkinitcpio preset file for the 'linux-zen' package

	 #ALL_config="/etc/mkinitcpio.conf"
	-ALL_kver="/boot/vmlinuz-%PKGBASE%"
	+ALL_kver="/boot/vmlinuz-linux-zen"

	 PRESETS=('default' 'fallback')

	 #default_config="/etc/mkinitcpio.conf"
	-default_image="/boot/initramfs-%PKGBASE%.img"
	-#default_uki="/efi/EFI/Linux/arch-%PKGBASE%.efi"
	+default_image="/boot/initramfs-linux-zen.img"
	+#default_uki="/efi/EFI/Linux/arch-linux-zen.efi"
	 #default_options="--splash /usr/share/systemd/bootctl/splash-arch.bmp"

	 #fallback_config="/etc/mkinitcpio.conf"
	-fallback_image="/boot/initramfs-%PKGBASE%-fallback.img"
	-#fallback_uki="/efi/EFI/Linux/arch-%PKGBASE%-fallback.efi"
	+fallback_image="/boot/initramfs-linux-zen-fallback.img"
	+#fallback_uki="/efi/EFI/Linux/arch-linux-zen-fallback.efi"
	 fallback_options="-S autodetect"
EOF

cat >>"$(CreateFile /etc/sysctl.d/99-fs.conf)" <<-'EOF'
	fs.suid_dumpable = 0
EOF

cat >>"$(CreateFile /etc/sysctl.d/99-kernel.conf)" <<-'EOF'
	kernel.dmesg_restrict = 1
	kernel.kptr_restrict = 2
	kernel.sysrq = 0
EOF

cat >>"$(CreateFile /etc/sysctl.d/99-net.conf)" <<-'EOF'
	net.ipv4.conf.all.accept_redirects = 0
	net.ipv4.conf.all.log_martians = 1
	net.ipv4.conf.all.rp_filter = 1
	net.ipv4.conf.all.send_redirects = 0
	net.ipv4.conf.default.accept_redirects = 0
	net.ipv4.conf.default.accept_source_route = 0
	net.ipv4.conf.default.log_martians = 1
	net.ipv6.conf.all.accept_redirects = 0
	net.ipv6.conf.default.accept_redirects = 0
EOF

# shellcheck shell=bash

AddPackage \
		aconfmgr-git \
			patch \
		base \
			dbus-broker-units \
		colordiff \
		logrotate \
		sudo

CreateLink /etc/os-release ../usr/lib/os-release

cat >>"$(CreateFile /etc/sudoers.d/00-wheel 440)" <<-'EOF'
	%wheel ALL=(ALL) ALL
EOF

CopyFile /usr/local/bin/aconfdiff 755

# shellcheck shell=bash

AddPackage \
		grml-zsh-config \
		zsh \
		zsh-completions

CopyFile /etc/profile.d/tmout.sh

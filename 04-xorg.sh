# shellcheck shell=bash

AddPackage \
		xf86-video-amdgpu

cat >>"$(CreateFile /etc/X11/xorg.conf.d/00-keyboard.conf)" <<-'EOF'
	Section "InputClass"
	  Identifier "Keyboard"
	  MatchIsKeyboard "on"
	  Option "XkbLayout" "fr"
	  Option "XkbVariant" "oss"
	EndSection
EOF

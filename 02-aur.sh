# shellcheck shell=bash

AddPackage \
		aurutils \
			devtools \
			vifm

patch "$(GetPackageOriginalFileTo devtools /usr/share/devtools/pacman.conf.d/extra.conf /etc/aurutils/pacman-x86_64.conf)" <<-EOF
	@@ -30,12 +30,12 @@

	 # Misc options
	 #UseSyslog
	-#Color
	+Color
	 NoProgressBar
	 # We cannot check disk space from within a chroot environment
	 #CheckSpace
	 VerbosePkgLists
	-ParallelDownloads = 5
	+ParallelDownloads = 4
	 DownloadUser = alpm
	 #DisableSandbox

	@@ -89,3 +89,7 @@
	 #[custom]
	 #SigLevel = Optional TrustAll
	 #Server = file:///home/custompkgs
	+
	+[aur]
	+SigLevel = Optional TrustAll
	+Server = file:///var/local/aur
EOF

patch "$(GetPackageOriginalFile pacman /etc/makepkg.conf)" <<-EOF
	@@ -49,7 +49,7 @@
	          -Wl,-z,pack-relative-relocs"
	 LTOFLAGS="-flto=auto"
	 #-- Make Flags: change this for DistCC/SMP systems
	-#MAKEFLAGS="-j2"
	+MAKEFLAGS="${MAKE_FLAGS}"
	 #-- Debugging flags
	 DEBUG_CFLAGS="-g"
	 DEBUG_CXXFLAGS="\$DEBUG_CFLAGS"
	@@ -95,7 +96,7 @@
	 #-- lto:        Add compile flags for building with link time optimization
	 #-- autodeps:   Automatically add depends/provides
	 #
	-OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge debug lto)
	+OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug lto)

	 #-- File integrity checks to use. Valid: md5, sha1, sha224, sha256, sha384, sha512, b2
	 INTEGRITY_CHECK=(sha256)
EOF

cat >>"$(CreateFile /etc/sudoers.d/00-aur 440)" <<-'EOF'
	aur ALL=(root) NOPASSWD: /usr/bin/arch-nspawn, NOPASSWD:SETENV: /usr/bin/makechrootpkg
EOF

CopyFile /usr/local/bin/aur-remove 755

patch "$(GetPackageOriginalFile devtools /usr/share/devtools/makepkg.conf.d/x86_64.conf)" <<-EOF
	@@ -49,7 +49,7 @@
	          -Wl,-z,pack-relative-relocs"
	 LTOFLAGS="-flto=auto"
	 #-- Make Flags: change this for DistCC/SMP systems
	-#MAKEFLAGS="-j2"
	+MAKEFLAGS="${MAKE_FLAGS}"
	 #-- Debugging flags
	 DEBUG_CFLAGS="-g"
	 DEBUG_CXXFLAGS="\$DEBUG_CFLAGS"
	@@ -95,7 +95,7 @@
	 #-- lto:        Add compile flags for building with link time optimization
	 #-- autodeps:   Automatically add depends/provides
	 #
	-OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge debug lto)
	+OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug lto)

	 #-- File integrity checks to use. Valid: md5, sha1, sha224, sha256, sha384, sha512, b2
	 INTEGRITY_CHECK=(sha256)
EOF

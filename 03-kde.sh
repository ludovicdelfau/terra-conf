# shellcheck shell=bash

AddPackage \
		plasma-meta \
			flatpak-kcm \
			packagekit-qt6 \
			phonon-qt6-vlc \
			qt6-multimedia-ffmpeg

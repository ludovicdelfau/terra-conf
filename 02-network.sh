# shellcheck shell=bash

AddPackage \
		bluez \
		ethtool \
		networkmanager \
		zerotier-one \
		zerotier-systemd-manager-bin

CreateLink /etc/resolv.conf "${RESOLV_CONF}"

cat >>"$(CreateFile /etc/hostname)" <<-'EOF'
	terra
EOF

patch "$(GetPackageOriginalFile filesystem /etc/hosts)" <<-'EOF'
	@@ -1,2 +1,7 @@
	 # Static table lookup for hostnames.
	 # See hosts(5) for details.
	+127.0.0.1   localhost.localdomain localhost
	+::1         localhost.localdomain localhost
	+
	+127.0.1.1   terra.localdomain     terra
	+192.168.0.2 mars.localdomain      mars
EOF

patch "$(GetPackageOriginalFile iptables /etc/iptables/iptables.rules)" <<-'EOF'
	@@ -1,6 +1,23 @@
	-# Empty iptables rule file
	 *filter
	-:INPUT ACCEPT [0:0]
	-:FORWARD ACCEPT [0:0]
	+:INPUT DROP [0:0]
	+:FORWARD DROP [0:0]
	 :OUTPUT ACCEPT [0:0]
	+:TCP - [0:0]
	+:UDP - [0:0]
	+-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
	+-A INPUT -i lo -j ACCEPT
	+-A INPUT -m conntrack --ctstate INVALID -j DROP
	+-A INPUT -p icmp -m icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
	+-A INPUT -p udp -m conntrack --ctstate NEW -j UDP
	+-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP
	+-A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
	+-A INPUT -p tcp -j REJECT --reject-with tcp-reset
	+-A INPUT -j REJECT --reject-with icmp-proto-unreachable
	+# SSH
	+-A TCP -p tcp --dport 22 -j ACCEPT
	+# Sunshine
	+-A UDP -p udp --dport 47998:48000 -j ACCEPT
	+-A TCP -p tcp --dport 47984 -j ACCEPT
	+-A TCP -p tcp --dport 47989 -j ACCEPT
	+-A TCP -p tcp --dport 48010 -j ACCEPT
	 COMMIT
EOF

patch "$(GetPackageOriginalFile iptables /etc/iptables/ip6tables.rules)" <<-'EOF'
	@@ -1,6 +1,18 @@
	-# Empty iptables rule file
	 *filter
	-:INPUT ACCEPT [0:0]
	-:FORWARD ACCEPT [0:0]
	+:INPUT DROP [0:0]
	+:FORWARD DROP [0:0]
	 :OUTPUT ACCEPT [0:0]
	+:TCP - [0:0]
	+:UDP - [0:0]
	+-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
	+-A INPUT -i lo -j ACCEPT
	+-A INPUT -m conntrack --ctstate INVALID -j DROP
	+-A INPUT -s fe80::/10 -p ipv6-icmp -j ACCEPT
	+-A INPUT -p udp --sport 547 --dport 546 -j ACCEPT
	+-A INPUT -p udp -m conntrack --ctstate NEW -j UDP
	+-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP
	+-A INPUT -p udp -j REJECT --reject-with icmp6-adm-prohibited
	+-A INPUT -p tcp -j REJECT --reject-with tcp-reset
	+-A INPUT -j REJECT --reject-with icmp6-adm-prohibited
	+-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -m conntrack --ctstate NEW -j ACCEPT
	 COMMIT
EOF

patch "$(GetPackageOriginalFileTo systemd /usr/lib/systemd/system/systemd-networkd-wait-online.service /etc/systemd/system/systemd-networkd-wait-online.service)" <<-EOF
	@@ -19,7 +19,7 @@

	 [Service]
	 Type=oneshot
	-ExecStart=/usr/lib/systemd/systemd-networkd-wait-online
	+ExecStart=/usr/lib/systemd/systemd-networkd-wait-online --interface=eno1
	 RemainAfterExit=yes

	 [Install]
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-network.preset)" <<-'EOF'
	enable bluetooth.service
	enable iptables.service
	enable ip6tables.service
	enable NetworkManager.service
	enable zerotier-one.service
	enable zerotier-systemd-manager.timer
EOF

cat >>"$(CreateFile /etc/udev/rules.d/99-wol.rules)" <<-'EOF'
	ACTION=="add", SUBSYSTEM=="net", NAME=="en*", RUN+="/usr/bin/ethtool -s $name wol g"
EOF

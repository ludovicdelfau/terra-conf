# shellcheck shell=bash

AddPackage \
		wine \
			samba \
		wine-gecko \
		wine-mono \
		winetricks

CreateLink /etc/fonts/conf.d/30-win32-aliases.conf ../../../usr/share/fontconfig/conf.default/30-win32-aliases.conf

# shellcheck shell=bash

AddPackage \
		openssh \
		ssh-audit

patch "$(GetPackageOriginalFile openssh /etc/ssh/sshd_config)" <<-'EOF'
	@@ -16,9 +16,9 @@
	 #ListenAddress 0.0.0.0
	 #ListenAddress ::

	-#HostKey /etc/ssh/ssh_host_rsa_key
	+HostKey /etc/ssh/ssh_host_rsa_key
	 #HostKey /etc/ssh/ssh_host_ecdsa_key
	-#HostKey /etc/ssh/ssh_host_ed25519_key
	+HostKey /etc/ssh/ssh_host_ed25519_key

	 # Ciphers and keying
	 #RekeyLimit default none
	@@ -30,7 +30,7 @@
	 # Authentication:

	 #LoginGraceTime 2m
	-#PermitRootLogin prohibit-password
	+PermitRootLogin no
	 #StrictModes yes
	 #MaxAuthTries 6
	 #MaxSessions 10
	@@ -55,7 +55,8 @@
	 #IgnoreRhosts yes

	 # To disable tunneled clear text passwords, change to no here!
	-#PasswordAuthentication yes
	+PasswordAuthentication no
	+AuthenticationMethods publickey
	 #PermitEmptyPasswords no

	 # Change to no to disable s/key passwords
	@@ -115,3 +116,8 @@
	 #	AllowTcpForwarding no
	 #	PermitTTY no
	 #	ForceCommand cvs server
	+
	+# ssh-audit
	+Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
	+KexAlgorithms sntrup761x25519-sha512@openssh.com,curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha256
	+MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-ssh.preset)" <<-'EOF'
	enable sshd.service
EOF

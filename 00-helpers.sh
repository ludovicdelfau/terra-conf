# shellcheck shell=bash

GetPackageOriginalFileTo() {
	local package="${1}"
	local src_file="${2}"
	local dst_file="${3}"
	# shellcheck disable=SC2154
	local output_file="${output_dir}/files/${dst_file}"
	mkdir --parents "$(dirname "${output_file}")"
	AconfGetPackageOriginalFile "${package}" "${src_file}" > "${output_file}"
	printf '%s' "${output_file}"
}

filter_patch_output() {
	local file="${1}"
	local state=0
	while IFS= read -r line; do
		case ${state} in
		0)
			if [ "${line}" = "patching file ${file}" ]; then
				first_line="${line}"
				state=1
			else
				echo "${line}"
				state=2
			fi
			;;
		1)
			echo "${first_line}"
			echo "${line}"
			state=2
			;;
		2)
			echo "${line}"
			;;
		esac
	done
}

patch() {
	local file="${1}"
	env patch --no-backup-if-mismatch "${file}" 2>&1 | filter_patch_output "${file}"
}

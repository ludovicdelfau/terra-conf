# shellcheck shell=sh

if [ "$(id -un)" != "$(logname)" ]; then
	export TMOUT=300
fi

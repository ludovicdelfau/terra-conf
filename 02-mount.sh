# shellcheck shell=bash

AddPackage \
		dosfstools \
		e2fsprogs

CreateLink /etc/systemd/system-generators/systemd-gpt-auto-generator /dev/null

cat >>"$(CreateFile /etc/systemd/system/dev-disk-by\\x2dlabel-swap.swap)" <<-EOF
	[Unit]
	Before=swap.target

	[Mount]
	What=/dev/disk/by-label/swap
	Options=${MOUNT_OPTIONS[swap]}

	[Install]
	RequiredBy=swap.target
EOF

cat >>"$(CreateFile /etc/systemd/system/boot.mount)" <<-EOF
	[Unit]
	Before=local-fs.target
	Requires=systemd-fsck@dev-disk-by\x2dlabel-boot.service
	After=systemd-fsck@dev-disk-by\x2dlabel-boot.service

	[Mount]
	What=/dev/disk/by-label/boot
	Where=/boot
	Type=vfat
	Options=${MOUNT_OPTIONS[boot]}

	[Install]
	RequiredBy=local-fs.target
EOF

cat >>"$(CreateFile /etc/systemd/system/var.mount)" <<-EOF
	[Unit]
	Before=local-fs.target
	Requires=systemd-fsck@dev-disk-by\x2dlabel-arch\x2dvar.service
	After=systemd-fsck@dev-disk-by\x2dlabel-arch\x2dvar.service

	[Mount]
	What=/dev/disk/by-label/arch-var
	Where=/var
	Type=ext4
	Options=${MOUNT_OPTIONS[arch-var]}

	[Install]
	RequiredBy=local-fs.target
EOF

cat >>"$(CreateFile /etc/systemd/system/home.mount)" <<-EOF
	[Unit]
	Before=local-fs.target
	Requires=systemd-fsck@dev-disk-by\x2dlabel-arch\x2dhome.service
	After=systemd-fsck@dev-disk-by\x2dlabel-arch\x2dhome.service

	[Mount]
	What=/dev/disk/by-label/arch-home
	Where=/home
	Type=ext4
	Options=${MOUNT_OPTIONS[arch-home]}

	[Install]
	RequiredBy=local-fs.target
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-mount.preset)" <<-'EOF'
	enable dev-disk-by\x2dlabel-swap.swap
	enable boot.mount
	enable var.mount
	enable home.mount
	enable fstrim.timer
EOF

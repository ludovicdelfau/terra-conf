# shellcheck shell=bash

# shellcheck disable=SC2016,SC2034
ARCH_MIRROR='https://archlinux.mailtunnel.eu/$repo/os/$arch'

# shellcheck disable=SC2034
declare -A MOUNT_OPTIONS=(
	[boot]='defaults,dmask=0022,fmask=0133'
	[swap]='defaults'
	[arch]='defaults'
	[arch-var]='defaults'
	[arch-home]='defaults'
)

# shellcheck disable=SC2034
RESOLV_CONF='../run/systemd/resolve/stub-resolv.conf'

# shellcheck disable=SC2034
MAKE_FLAGS='-j16'

# shellcheck shell=bash

AddPackage \
		sunshine \
			libva-mesa-driver

patch "$(GetPackageOriginalFileTo systemd /usr/lib/systemd/system/getty@.service /etc/systemd/system/getty1-autologin-wayland@.service)" <<-'EOF'
	@@ -8,7 +8,7 @@
	 #  (at your option) any later version.

	 [Unit]
	-Description=Getty on %I
	+Description=Getty on tty1 with automatic login of %I in a Wayland session
	 Documentation=man:agetty(8) man:systemd-getty-generator(8)
	 Documentation=https://0pointer.de/blog/projects/serial-console.html
	 After=systemd-user-sessions.service plymouth-quit-wait.service getty-pre.target
	@@ -33,14 +33,15 @@
	 [Service]
	 # The '-o' option value tells agetty to replace 'login' arguments with '--' for
	 # safety, and then the entered username.
	-ExecStart=-/sbin/agetty -o '-- \\u' --noreset --noclear - ${TERM}
	+ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noreset --noclear --autologin %I - $TERM
	+Environment=XDG_SESSION_TYPE=wayland
	 Type=idle
	 Restart=always
	 RestartSec=0
	-UtmpIdentifier=%I
	+UtmpIdentifier=tty1
	 StandardInput=tty
	 StandardOutput=tty
	-TTYPath=/dev/%I
	+TTYPath=/dev/tty1
	 TTYReset=yes
	 TTYVHangup=yes
	 TTYVTDisallocate=yes
	@@ -55,7 +56,3 @@
	 # Unset locale for the console getty since the console has problems
	 # displaying some internationalized messages.
	 UnsetEnvironment=LANG LANGUAGE LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT LC_IDENTIFICATION
	-
	-[Install]
	-WantedBy=getty.target
	-DefaultInstance=tty1
EOF

patch "$(GetPackageOriginalFileTo systemd /usr/lib/systemd/system/systemd-reboot.service /etc/systemd/system/systemd-reboot-windows.service)" <<-'EOF'
	@@ -8,9 +8,13 @@
	 #  (at your option) any later version.

	 [Unit]
	-Description=System Reboot
	+Description=System Reboot into Windows
	 Documentation=man:systemd-reboot.service(8)
	 DefaultDependencies=no
	 Requires=shutdown.target umount.target final.target
	 After=shutdown.target umount.target final.target
	 SuccessAction=reboot-force
	+
	+[Service]
	+ExecStart=-/usr/bin/efibootmgr -n 0005
	+Type=oneshot
EOF

cat >>"$(CreateFile /etc/systemd/system-preset/00-sunshine.preset)" <<-'EOF'
	enable avahi-daemon.service
EOF
